package src.Controller;

import src.Model.Model;
import src.View.GameGUI;
import src.list.Datenelement;

public class GameController implements Datenelement
{
    private GameGUI gGUI;
    private Model main;

    /**
     * Konstruktor erstellt neues Model (LJ)
     * @param ist das neue Model, das erstellt werden soll
     */
    public GameController(Model newMain)
    {
        main = newMain;
    }

    /**
     * Methode, die überprüft, ob die gGUI angezeigt wird (wenn der State des Models 1 ist). 
     * Falls keine gGUI besteht, erzeugt man eine Neue. 
     * Wenn der State nicht 1 ist, so soll die gGUI nicht angezeigt werden, sondern die mmGUI.
     */
    public void update()
    {
        if(main.getGameState() == 1)
        {
            if(gGUI == null)
            {
                gGUI = new GameGUI(main,this);

            }
            gGUI.update();     
        }  
        else
        {   
            if(gGUI != null)
            {
                visible(false);
            }
            gGUI = null;
        }
    }

    /**
     * Gibt an, ob die gGUI im Moment angezeigt wird. 
     * (Die Methode gibt visible() an die gGUI weiter, mit einem Boolean als Mitgabewert.)
     */
    public void visible(boolean wert)
    {
        gGUI.visible(wert);
    }
    
    public String getUeberschrift(){
        return this.main.getUeberschrift();
    }

    /**
     * Vollständigkeitshalber
     */
    public void updateMain(int i)
    {
        
    }
    
    public void backToMenu()
    {
        this.main.backToMenu();
    }

    /**
     * Fordert das Model dazu auf, den Buchstaben zu überprüfen.
     */
    public void textImput(String buchstabe)
    {
        main.textVergleich(buchstabe);
    }

    /**
     * @return gibt zurück, was von der GameGUI beim Aufrufen von timerSTop() zurückkommt (=benötigte Zeit)
     */
    public int getTimer()
    {
        return gGUI.timerStop();
    }
}
