package src.Controller;

import src.Model.Model;
import src.View.MainMenuGui;
import src.list.Datenelement;
import src.Model.*;

public class MainMenuController implements Datenelement
{
    private MainMenuGui mmGui;
    private Model main;
    private Datenbank db;
    
    /**
     * Konstruktor, der ein neues Model erzeugt.
     */
    public MainMenuController(Model newMain)
    {
        main = newMain;
    }
    
    /**
     * Methode, die überprüft, ob die mmGUI angezeigt wird (wenn der State des Models 0 ist). 
     * Falls keine mmGUI besteht, erzeugt man eine Neue. 
     * Wenn der State nicht 0 ist, so soll die mmGUI nicht angezeigt werden, sondern die gGUI.
     */
    public void update()
    {
        if(main.getGameState() == 0)
        {
            if(mmGui == null)
            {
                mmGui = new MainMenuGui(main,this);
            }
            mmGui.update();
        }
        else
        {
            mmGui = null;
        }
        
        if(main.getGameState() == 0 && main.benutzerGeben().getWPM() > 0)
        {
            db.insertUser(main.benutzerGeben());
        }
    }

    /**
     * @param zeigt, ob die mmGUI gerade angezeicht wird (wahr oder falsch)
     * Gibt an, ob die mmGUI im Moment angezeigt wird.
     * (Die Methode gibt visible() an die mmGUI weiter, mit einem Boolean als Mitgabewert.)
     */
    public void visible(boolean wert)
    {
        mmGui.visible(wert);
    }
    
    /**
     * Die mmGUI wird "unsichtbar" gemacht, damit das Model eine Aktion ausführen kann.
     */
    public void mainMenuStart()
    {        
        mmGui.visible(false);
        main.mainMenuStartAction();
    }

    /**
     * @return Zählt 20 Sekunden rückwärts
     */
    public int getTimer()
    {
        return -20;
    }

    /**
     * trennt die Verbindung zur Datenbank.
     */
    public void closeConnection() 
    {
        main.closeConnection();
    }

    /**
     * fügt den einen Benutzer in die Datenbank ein
     * @param bn Obkekt der Klasse Benutzer 
     */
    public void insertUser(Benutzer bn)
    {
        System.out.println(bn.getName()+" soll eingefügt werden");
        main.insertUser(bn);
    }


    /*--------------------------------------------------------------------------------------------
    *                                       for BestListGui
    *--------------------------------------------------------------------------------------------*/
    
    public Benutzer[] getTopTen()
    {
        return main.getTopTen();
    }

}
