package src.View;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import src.Controller.MainMenuController;
import src.Model.Model;  
import src.Model.Benutzer;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;

public class AnmeldeGUI extends JFrame 
{
    private JButton jButton1 = new JButton();
    private JLabel jLabel1 = new JLabel("", JLabel.CENTER);
    private JTextField textField1;
    private JTextField textField2;
    private JPasswordField jPassword1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private Model main;
    private MainMenuController controller;
    private BestListGUI bestlist;
    private String name;
    private static String benutzername;
    private String passwort;
    private int i = 0;  //Für Knopfdruck wichtig
    private Color rot = new Color(0xc40000);
        

    /**
     * Konsturktor
     * @param bekommt ein Model und einen MainMenuController mit
     */
    public AnmeldeGUI() 
    { 
        super();
        //main = newMain;
        //controller = newController;
        benutzername = "blank";
                
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        int frameWidth = 500; 
        int frameHeight = 600;
        int buttonWidth = 125;
        int buttonHeight = 50;
        int buttonYOffset = 195;
        int titleYOffset = 95;
        setSize(frameWidth, frameHeight);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width / 2) - (getSize().width / 2);
        int y = (d.height / 2) - (getSize().height / 2);
        setLocation(x, y);
        setTitle("TypeAndRun - Anmelden");
        jLabel1.setFont(new Font("Arial", Font.BOLD, 45));
        setResizable(false);
        Container cp = getContentPane();
        cp.setLayout(null);        

        jButton1.setBounds(frameWidth/2 - buttonWidth, buttonYOffset + buttonHeight*4, buttonWidth*2, buttonHeight);
        jButton1.setText("Bestätigen");
        jButton1.setMargin(new Insets(2, 2, 2, 2));
        jButton1.addActionListener(new ActionListener() 
            { 
                public void actionPerformed(ActionEvent evt) { 
                    jButton1_ActionPerformed(evt);
                }
            });
        cp.add(jButton1);
       
        jLabel1.setBounds(0, titleYOffset, frameWidth, 65);
        jLabel1.setText("Anmelden");
        cp.add(jLabel1);

        setVisible(true);
        
        textField1 = new JTextField();
        textField2 = new JTextField();
        jPassword1 = new JPasswordField(); //Vorher 1 in Klammer
        
        jLabel2 = new JLabel();
        jLabel3 = new JLabel();
        jLabel4 = new JLabel();
        jLabel5 = new JLabel("", JLabel.CENTER);        
        
        jLabel2.setBounds(50, 200, 200, 25);
        jLabel3.setBounds(50, 250, 200, 25);
        jLabel4.setBounds(50, 300, 200, 25);
        jLabel5.setBounds(frameWidth/2 - 400/2, 350, 400, 25);
        
        jLabel2.setText("Benutzername");
        cp.add(jLabel2);
        jLabel3.setText("Benutzername - MySQL");
        cp.add(jLabel3);
        jLabel4.setText("Passwort - MySQL");
        cp.add(jLabel4);
        jLabel5.setText("Fehler. Eingabe überprüfen.");
        jLabel5.setForeground(rot);
        jLabel5.setFont(new Font("Arial", Font.BOLD, 20)); 
        cp.add(jLabel5);
        jLabel5.setVisible(false);
        
        textField1.setBounds(200,200,250,25);
        textField1.getDocument().addDocumentListener(new DocumentListener(){
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void changedUpdate(DocumentEvent de) 
           {
        
           }
    
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void insertUpdate(DocumentEvent de) 
           {
               //textValueChanged();
           }
    
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void removeUpdate(DocumentEvent de) 
           {
               //this.main.fehlerWeniger();
           }
        
        }); 
        textField2.setBounds(200,250,250,25);
        textField2.getDocument().addDocumentListener(new DocumentListener(){
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void changedUpdate(DocumentEvent de) 
           {
        
           }
    
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void insertUpdate(DocumentEvent de) 
           {
               //textValueChanged();
           }
    
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void removeUpdate(DocumentEvent de) 
           {
               //this.main.fehlerWeniger();
           }
        
        });  
        jPassword1.setBounds(200,300,250,25);
        jPassword1.getDocument().addDocumentListener(new DocumentListener(){
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void changedUpdate(DocumentEvent de) 
           {
        
           }
    
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void insertUpdate(DocumentEvent de) 
           {
               //textValueChanged();
           }
    
           /**
           * Aufrufen der Methode textValueChanged().
           */
           public void removeUpdate(DocumentEvent de) 
           {
               //this.main.fehlerWeniger();
           }        
        });
        
        cp.add(textField1);
        cp.add(textField2);
        cp.add(jPassword1);
    }
         
    /**
     * Überprüft die angegebenen Anmeldedaten
     */
    public void jButton1_ActionPerformed(ActionEvent evt) 
    {  
        if(i == 0)
        {
            i++;
            benutzername = textField1.getText();
            name = textField2.getText();           
            passwort = jPassword1.getText();
            try 
            {
                if(name != null && benutzername != null && passwort != null)
                {
                    Model ov = new Model(name, passwort);
                    dispose();
                }
                else
                {
                    jLabel5.setVisible(true); //Funktioniert noch nicht!                   
                }
            }                    
            catch(Exception e)
            {
                e.printStackTrace();
                System.out.println("Fehler. Start nicht möglich"); 
                jLabel5.setVisible(true);
                i--;
            }
        }
        else
        {
        }
        benutzername = textField1.getText();
    }

    /**
     * 
     */
    public void update()
    {

    }

    /**
     * Setzt visible auf einen bestimmten Wert (wahr oder falsch).
     */
    public void visible(boolean wert)
    {
        this.setVisible(wert);
    }     
    
    public String nameAusgeben()
    {
        return name;
    }
    
    public String passwortAusgeben()
    {
        return passwort;
    }
    
    public static String benutzernameAusgeben()
    {
        return benutzername;
    }
    
} 

