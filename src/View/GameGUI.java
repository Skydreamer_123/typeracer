package src.View;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import src.Controller.GameController;
import src.Model.Model;
import src.Model.GameState;

public class GameGUI extends Frame implements DocumentListener
{
    private JLabel label1 = new JLabel();
    private JLabel label2 = new JLabel();
    private JTextField textField1;
    private String input;
    private int secondsLeft = 5;
    private int timerTime = 0;
    private Model main;
    private String eingegebenerText;
    private int alterTextZeichencount;
    private Color rot = new Color(0xc40000);
    private Color schwarz = new Color(0x000000);
    private Color grau = new Color(0x999999);
    private JButton button1;
    private GameState gs;
    
    private GameController controller;
    Timer timer = new Timer();
    TimerTask task = new TimerTask()
        {
            public void run() 
            {
                if(secondsLeft>0)
                {
                    String text1 = main.getText();
                    label1.setText("|" + text1);
                    label1.setForeground(grau);
                    secondsLeft = secondsLeft -1;
                    label2.setText(secondsLeft + " ");
                    textField1.setEditable(false);
                }
                else if(secondsLeft<=0)
                {
                    label1.setForeground(schwarz);
                    label2.setText("Type!");
                    textField1.setEditable(true);
                    newText();
                    timerTime = timerTime + 1;
                }
                
            }
        };
    
    /**
     * Konstruktor
     * @param bekommt ein Model und einen GameController mit
     */
    public GameGUI(Model newMain, GameController newController) 
    { 
        super();
        main = newMain;
        controller = newController;
        label1 = new JLabel();
        label2 = new JLabel();
        button1 = new JButton();
        textField1 = new JTextField(1);
        addWindowListener(new WindowAdapter() 
            {
                public void windowClosing(WindowEvent evt) {
                    dispose();
                }
            });
        int frameWidth = 1000;
        int frameHeight = 1000;
        int buttonWidth = 125;
        int bottomHeight = 50;
        int puffersize = 50;
        setSize(frameWidth, frameHeight);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width - getSize().width) / 2;
        int y = (d.height - getSize().height) / 2;
        setLocation(x, y);
        setTitle("TypeAndRun - Typing '" + this.controller.getUeberschrift() + "'");
        setResizable(false);
        
        
        Panel cp = new Panel(null);
        
        add(cp);

        label1.setBounds(50,50,900,50);
        label1.setText("lelele");
        label1.setHorizontalAlignment(SwingConstants.LEFT);
        label1.setForeground(grau);
        cp.add(label1);
        
        label2.setBounds(50,100,900,50);
        label2.setText("lelele");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        cp.add(label2);
        
        button1.setBounds(800,900,150,50);
        button1.setText("Back to Menu");
        //button1.setMargin(new Insets(2, 2, 2, 2));
        button1.addActionListener(new ActionListener() 
            { 
                public void actionPerformed(ActionEvent evt) { 
                    button1_ActionPerformed(evt);
                }
            });
        cp.add(button1);
        
        label1.setFont(new Font("Arial", Font.BOLD, 30));
        label2.setFont(new Font("Arial", Font.BOLD, 30));
        
        textField1.setBounds(50,900,750,51);
        textField1.getDocument().addDocumentListener(this); 
        textField1.setHighlighter(null);
        InputMap inputMap = textField1.getInputMap();
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK), "none");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.META_DOWN_MASK), "none");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, KeyEvent.CTRL_DOWN_MASK), "none");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, KeyEvent.META_DOWN_MASK), "none");

        textField1.addKeyListener(
            new KeyListener(){

                @Override
                public void keyTyped(KeyEvent e) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void keyPressed(KeyEvent e) {
                    // TODO Auto-generated method stub

                    String anz = textField1.getText();
                   
                    
                    if((e.getKeyCode() == KeyEvent.VK_LEFT) || (e.getKeyCode() == KeyEvent.VK_RIGHT) )
                    {
                        int laenge = anz.length();
                        System.out.println("LEFT or RIGHT IS PRESSED");
                        textField1.setCaretPosition(laenge);
                    }              
                    
                    if((e.getKeyCode() == KeyEvent.VK_BACK_SPACE) && (main.getFehler() == 0))
                    {
                        textField1.getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"), "none");
                    }
                    
                    
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    // TODO Auto-generated method stub                   
                }
            }
        );
        textField1.addMouseListener(
            new MouseListener(){
                
                @Override
                public void mouseClicked(MouseEvent e) {
                    // TODO Auto-generated method stub
                    textField1.setEditable(false);
                    textField1.setCaretPosition(textField1.getDocument().getLength());
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    // TODO Auto-generated method stub
                    textField1.setEditable(false);
                    textField1.setCaretPosition(textField1.getDocument().getLength());
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    // TODO Auto-generated method stub
                    textField1.setEditable(false);
                    textField1.setCaretPosition(textField1.getDocument().getLength());
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    // TODO Auto-generated method stub
                    textField1.setEditable(false);
                    textField1.setCaretPosition(textField1.getDocument().getLength());                
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    // TODO Auto-generated method stub

                }
            }
        );

        cp.add(textField1);
        setVisible(true);
        start();
    }
    
    /**
     * Hollt sich beim Controller einen neuen Text und 
     * setzt diesen dann in der Medthode setText().
     */
    public void newText()
    {
        String text1 = main.getText();
        label1.setText(text1.substring(0,this.main.getFehler()) + "|" + text1.substring(this.main.getFehler(),text1.length()));
        if(this.main.getFehler() > 0){
            label1.setForeground(rot);
        } else {
            label1.setForeground(schwarz);
        }
    }
    
    public void button1_ActionPerformed(ActionEvent evt){
        this.controller.backToMenu();
    }
    
    /**
     * Startet den Timer.
     */
    public void start()
    {
        timer.scheduleAtFixedRate(task, 0, 1000);
    }
    
    /**
     * @return Gibt den input zurück.
     */
    public JTextField getTextField()
    {
        return textField1;
    }
    
    /**
     * @return Gibt den input zurück.
     */
    public String getInput() 
    {
        return input;
    }
    
    /**
     * @param ist der Wert, auf den input gesetzt werden soll
     * Setzten den input auf einen bestimmten Wert (String)
     */
    public void setInput(String input) 
    {
        this.input = input;
    }

    /**
     * Aufrufen der Methode textValueChanged().
     */
    public void changedUpdate(DocumentEvent de) 
    {
        
    }
    
    /**
     * Aufrufen der Methode textValueChanged().
     */
    public void insertUpdate(DocumentEvent de) 
    {
        textValueChanged();
    }
    
    /**
     * Aufrufen der Methode textValueChanged().
     */
    public void removeUpdate(DocumentEvent de) 
    {
        this.main.fehlerWeniger();
    }
    
    /**
     * Speichert den letzten eingegebenen Buchstaben ab 
     * und gibt ihn daraufhin durch die Methode textImput() 
     * an den Controller weiter.
     */
    private void textValueChanged() 
    {
        try 
        {
            if(this.main.getFehler() == 0){
                eingegebenerText = textField1.getText();
                String vergleichsbuchstabe = eingegebenerText.substring(eingegebenerText.length() -1 ,eingegebenerText.length());
                controller.textImput(vergleichsbuchstabe);               
                
            } else {
                this.main.fehlerMehr();
                textField1.getInputMap().remove(KeyStroke.getKeyStroke("BACK_SPACE"));
            }
        }
        catch (NumberFormatException x) 
        {
            //  irgendetwas
        }
        catch (Exception x) 
        {
            //  irgendetwas
        }
    }
    
    /**
     * Ruft die Methode newText() in dieser Klasse auf.
     */
    public void update()
    {
        this.newText();   
    }
    
    /**
     * Setzt visible auf einen bestimmten Wert (wahr oder falsch).
     */
    public void visible(boolean wert)
    {
        setVisible(wert);
    }

    /**
     * stoppt den Timer
     * @return gibt die benötigte/gestoppte Zeit zurück
     */
    public int timerStop()
    {
        timer.cancel();
        task.cancel();
        return timerTime;
    }
}

