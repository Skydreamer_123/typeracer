package src.View;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.WindowConstants;

import src.Controller.MainMenuController;
import src.Model.Benutzer;
import src.Model.Model; 

public class BestListGUI extends JFrame
{
    private JButton jButton1 = new JButton();
    private JLabel jLabel1 = new JLabel("", JLabel.CENTER);
    private JLabel[] jLabels;
    private Model main;
    private MainMenuController mainController;   
    private Benutzer[] benutzers;

    /**
     * Konsturktor
     * @param bekommt ein Model und einen MainMenuController mit
     */
    public BestListGUI(Model newMain, MainMenuController newController) 
    { 
        super();
        main = newMain;
        mainController = newController;
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        int frameWidth = 500; 
        int frameHeight = 600;
        int buttonWidth = 125;
        int buttonHeight = 50;
        int buttonYOffset = 500;
        int titleYOffset = 105;
        benutzers = new Benutzer[10];
        jLabels = new JLabel[10];
        setSize(frameWidth, frameHeight);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width / 2) - (getSize().width / 2);
        int y = (d.height / 2) - (getSize().height / 2);
        setLocation(x, y);
        setTitle("TypeAndRun - Main Menu");
        jLabel1.setFont(new Font("Arial", Font.ITALIC, 30));
        setResizable(false);
        Container cp = getContentPane();
        cp.setLayout(null);
        jButton1.setBounds(frameWidth/2 - buttonWidth, buttonYOffset, buttonWidth*2, buttonHeight);
        jButton1.setText("Back to Main Menu");
        jButton1.setMargin(new Insets(2, 2, 2, 2));
        jButton1.addActionListener(new ActionListener() 
            { 
                public void actionPerformed(ActionEvent evt) { 
                    jButton1_ActionPerformed(evt);
                }
            });
        cp.add(jButton1);
              
        jLabel1.setBounds(0, titleYOffset-30, frameWidth, 65);
        jLabel1.setText("Bestenliste");
        cp.add(jLabel1); 

        for(int i = 0; i < jLabels.length; i++) {
            jLabels[i] = new JLabel("-----------------------", JLabel.CENTER);
            jLabels[i].setBounds(20 , 105 + (20*i), frameWidth - 20 , 65 + (20*i));
            cp.add(jLabels[i]);
        }
        
        setTopTen();
        setVisible(true);
    }

    /**
     * Ruft beim Controller die Methode meinMenuButton() auf.
     */
    public void jButton1_ActionPerformed(ActionEvent evt) 
    {
        mainController.visible(true);
        this.visible(false);
    }

    /**
     * wird benötigt wegen der Liste
     */
    public void update()
    {

    }

    /**
     * zeigt die 10 besten Spieler aus der Datenbank im BestListGui an
     */
    public void setTopTen()
    {    
        this.resetUsers();
        this.resetList();

        this.benutzers = mainController.getTopTen();
        
        if(this.benutzers != null)
        {              
            for (int i = 0; i < this.benutzers.length; i++) {
                if(this.benutzers[i] != null)
                {
                    this.jLabels[i-1].setText(this.benutzers[i].getName() + " has scored a Words per minute of " + this.benutzers[i].getWPM());
                    //System.out.println(benutzers[i].getName() + " has scored a Words per minute of " + benutzers[i].getWPM());
                }
            }
        }
        else
        {
            System.out.println("Datenbank mit Benutzern ist leer");
        }
    }

    /**
     * Setzt visible auf einen bestimmten Wert (wahr oder falsch).
     */
    public void visible(boolean wert)
    {
        this.setVisible(wert);
    }
    /**
     * set the text of all Jlabels to the standart text
     */
    public void resetList()
    {
        for (int i = 0; i < jLabels.length; i++) {
            jLabels[i].setText("-----------------------");
        }
    }
    /**
     * resets the 10 best playes and set all elements of the array benuter to null
     */
    public void resetUsers()
    {
        for (int i = 0; i < this.benutzers.length; i++) 
        {
            this.benutzers[i] = null;
        }
    }
} 

