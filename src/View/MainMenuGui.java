package src.View;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import src.Controller.MainMenuController;
import src.Model.Model;  
import src.Model.Benutzer;
import src.Model.Datenbank;

public class MainMenuGui extends JFrame
{
    private JButton jButton1 = new JButton();
    private JButton jButton2 = new JButton();
    private JButton jButton3 = new JButton();
    private JButton jButton4 = new JButton();
    private JLabel jLabel1 = new JLabel("", JLabel.CENTER);
    private JLabel jLabel2 = new JLabel();
    private JLabel jLabel3 = new JLabel();
    private JLabel jLabel6 = new JLabel("", JLabel.CENTER);
    private Model main;
    private Benutzer bn;
    private MainMenuController controller;
    private BestListGUI bestListGUI;
    private AnmeldeGUI anGUI;  

    /**
     * Konsturktor
     * @param bekommt ein Model und einen MainMenuController mit
     */
    public MainMenuGui(Model newMain, MainMenuController newController) 
    { 
        super();
        main = newMain;
        controller = newController;
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        int frameWidth = 500; 
        int frameHeight = 600;
        int buttonWidth = 125;
        int buttonHeight = 50;
        int buttonYOffset = 195;
        int titleYOffset = 95;
        setSize(frameWidth, frameHeight);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width / 2) - (getSize().width / 2);
        int y = (d.height / 2) - (getSize().height / 2);
        setLocation(x, y);
        setTitle("TypeAndRun - Main Menu");
        jLabel1.setFont(new Font("Arial", Font.ITALIC, 30));
        setResizable(false);
        Container cp = getContentPane();
        cp.setLayout(null);
        jButton1.setBounds(frameWidth/2 - buttonWidth, buttonYOffset, buttonWidth*2, buttonHeight);
        jButton1.setText("Start");
        jButton1.setMargin(new Insets(2, 2, 2, 2));
        jButton1.addActionListener(new ActionListener() 
            { 
                public void actionPerformed(ActionEvent evt) { 
                    jButton1_ActionPerformed(evt);
                }
            });
        cp.add(jButton1);
        /*
        jButton2.setBounds(frameWidth/2 - buttonWidth, buttonYOffset + buttonHeight, buttonWidth, buttonHeight);
        jButton2.setText("Credits");
        jButton2.setMargin(new Insets(2, 2, 2, 2));
        jButton2.addActionListener(new ActionListener() 
            { 
                public void actionPerformed(ActionEvent evt) { 
                    jButton2_ActionPerformed(evt);
                }
            });
        cp.add(jButton2);
        */
        
        jButton3.setBounds(frameWidth/2 - buttonWidth, buttonYOffset + buttonHeight, buttonWidth*2, buttonHeight);
        jButton3.setText("Score");
        jButton3.setMargin(new Insets(2, 2, 2, 2));
        jButton3.addActionListener(new ActionListener() 
            { 
                public void actionPerformed(ActionEvent evt) { 
                    jButton3_ActionPerformed(evt);
                }
            });
        cp.add(jButton3);
        
        jButton4.setBounds(frameWidth/2 - buttonWidth, buttonYOffset + buttonHeight*2, buttonWidth*2, buttonHeight);
        jButton4.setText("Exit");
        jButton4.setMargin(new Insets(2, 2, 2, 2));
        jButton4.addActionListener(new ActionListener() 
            { 
                public void actionPerformed(ActionEvent evt) { 
                    jButton4_ActionPerformed(evt);
                }
            });
        cp.add(jButton4);
       
        jLabel1.setBounds(0, titleYOffset, frameWidth, 65);
        jLabel1.setText("TypeAndRun");
        cp.add(jLabel1);
        
        jLabel6.setBounds(0, titleYOffset+55, frameWidth, 30);
        jLabel6.setText("Willkommen "+ AnmeldeGUI.benutzernameAusgeben());
        cp.add(jLabel6);
        
        if(main.getLetzteZeit() > 0)
        {
            jLabel2.setBounds(frameWidth/2 - 200, 450, 400, 25);
            jLabel2.setText(main.getGeschwindigkeit() + " Anschläge pro Minute");
            jLabel2.setFont(new Font("Arial", Font.ITALIC, 20));
            cp.add(jLabel2);
            
            jLabel3.setBounds(frameWidth/2 - 100, 400, 200, 25);
            jLabel3.setText("Geschwindigkeit: ");
            jLabel3.setFont(new Font("Arial", Font.ITALIC, 20));
            cp.add(jLabel3);

            try
            {
                Benutzer neu = new Benutzer(AnmeldeGUI.benutzernameAusgeben(), main.getGeschwindigkeit());
                controller.insertUser(neu);
            }
            catch(Exception e)
            {
                e.printStackTrace();
                System.err.println(e);
            }
        }   
        setVisible(true);
        //controller.closeConnection();
    }

    /**
     * Ruft beim Controller die Methode meinMenuButton() auf.
     */
    public void jButton1_ActionPerformed(ActionEvent evt) 
    {
        controller.mainMenuStart();    
    }
    
    /**
     * Soll die Credits (also die namen von uns allen) in einem seperaten Fenster zeigen.
     */
    public void jButton2_ActionPerformed(ActionEvent evt) 
    {
        main.benutzerGeben();
    }
    
    /**
     * Soll die Bestenliste in einem seperaten Fenster anzeigen.
     */
    public void jButton3_ActionPerformed(ActionEvent evt) 
    {    
        bestListGUI = new BestListGUI(main, controller);
        visible(false);
    }
    
    /**
     * Schließt das Spiel beim Drücken des Knopfes "Exit".
     */
    public void jButton4_ActionPerformed(ActionEvent evt) 
    {
        controller.closeConnection();
        System.exit(0);
    }

    /**
     * wegen Liste notwendig
     */
    public void update()
    {
        
    }

    /**
     * Setzt visible auf einen bestimmten Wert.
     */
    public void visible(boolean wert)
    {
        this.setVisible(wert);
    }       
} 

