package src.list;
public interface Datenelement
{
    /**
     * Abstrakte Update-Methode für Datenelemente.
     * sts
     */
     public void update();   
     
    public int getTimer();
}