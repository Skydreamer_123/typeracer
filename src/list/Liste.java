package src.list;
public class Liste
{
    private Listenelement anfang;
    /**
     * Konstruktur zur Initialisierung der Liste; aufgerufen durch Model.
     * @Konstruktur Initialisierung der Liste; @Konstruktor aufgerufen durch Model
     * sts
     */
    public Liste()
    {
        Abschluss abschluss = new Abschluss();
        anfang = abschluss;
    }

    /**
     * Einfügen eines Observer-Objektes in die Liste. Es werden alle Controller und Views eingefügt.
     * @param dneu Observer-Objekt
     * sts
     */
    public void attach(Datenelement dneu)
    {
        Knoten kneu = new Knoten(dneu, anfang);
        anfang = kneu;
    }

    /**
     * Entfernen eines Observer-Objektes aus der Liste. In der Regel nicht erforderlich.
     * @param dvergleich Observer-Objekt
     * sts
     */
    public void knotenEntfernen(Datenelement dvergleich)
    {    
        anfang = anfang.knotenEntfernen(dvergleich);
    }
    
    /**
    * Benachrichtigen aller Listenelemente, die eingefügt sind, dass ein Update im Model verfügbar ist.
    * sts
    */
    public void notifyObserver()
    {
        anfang.notifyObserver();    
    }
    
    public int getTimer()
    {
        return anfang.getTimer();
    }
}
