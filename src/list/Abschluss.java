package src.list;

import src.Model.Model;

public class Abschluss extends Listenelement
{
    /**
     * Leerer Konstruktor, keine Initialisierungen nötig.
     * sts
     */
    public Abschluss()
    {
        //this is the constructor
    }
    
    /**
     * Methode kann keinen Knoten entfernen, da Ende der Liste.
     * @param dvergleich zu entfernendes Observer-Objekt
     * @return Reihenfolge
     * sts
     */
    public Listenelement knotenEntfernen(Datenelement dvergleich)
    {
        return this;
    }
    
    /**
     * Benachrichtigung. Methode hat keine Aufgabe mehr, da Ende der Liste.
     * sts
     */
    public void notifyObserver()
    {
        
    }
    
    public void attachMain(Model m1)
    {
        
    }
    
    public int getTimer()
    {
        return -20;
    }
}