package src.list;
public abstract class Listenelement
{
    /**
     * Abstrakter Konstruktor, kein Inhalt.
     * sts
     */
    Listenelement() {}
   
    /**
     * Abstrakte Methode zum Entfernen der Knoten.
     * @param dvergleich Zu entfernendes DElement
     * @return Rückgabe der neuen Listenreihenfolge
     * sts
     */
    public abstract Listenelement knotenEntfernen(Datenelement dvergleich);
    
    /**
     * Benachrichtigungsmethode.
     * sts
     */
    public abstract void notifyObserver();
    
    public abstract int getTimer();
}