package src.list;
public class Knoten extends Listenelement
{
    private Listenelement nachfolger;
    private Datenelement delement;

    /**
     * Konstruktor zur Initialisierung.
     * @param dneu zu verwaltendes DElement
     * @param nfolger Nachfolger
     * sts
     */
    Knoten(Datenelement dneu, Listenelement nfolger)
    {
        delement = dneu;
        nachfolger = nfolger;
    }   

    /**
     * Entfernen eines, eventuell dieses Knotens.
     * @param dvergleich zu entfernendes DElement
     * @return Rückgabe der korrekten Listenreihenfolge
     * sts
     */
    public Listenelement knotenEntfernen(Datenelement dvergleich)
    {
        if(delement == dvergleich)
        {
            return nachfolger;
        }
        else
        {
            nachfolger= nachfolger.knotenEntfernen(dvergleich);
            return this;
        }
    }

    /**
     * Benachrichtigen des Datenelementes für Update.
     * sts
     */
    public void notifyObserver()
    {
        delement.update();
        nachfolger.notifyObserver();
    }
    
    public int getTimer()
    {
        if(delement.getTimer() != -20)
        {
            return delement.getTimer();    
        }
        else
        {
            return nachfolger.getTimer();
        }
    }
}