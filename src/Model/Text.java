package src.Model;

import java.sql.ResultSet;

public class Text 
{
    private String text;
    private String ueberschrift;
    private int textlaenge;
    private Datenbank db;
    
    /**
     * Zufällige Auswahl einer beliebigen Spalte der Tabelle Text aus der Datenbank
     * holt sich ID, Überschrift, Text und Schwierigkeit von der Datenbank und speichert dies als Objekt
     */
    public Text()
    {
        db = Datenbank.getInstance();
        String abfrage = "select * from texte ORDER BY RAND() LIMIT 1";
        ResultSet rs = db.abrufen(abfrage);
        try {
            while(rs.next())
            {
                /*
                *   speichert die Werte von der Tabelle ab.
                */
                String id = rs.getString("id");
                this.ueberschrift = rs.getString("ueberschrift");  
                this.text = rs.getString("text"); 
                this.textlaenge = rs.getInt("textlaenge");
                int schwierigkeit = rs.getInt("schwierigkeit");
                //System.out.println(id + " " + ueberschrift + " " + text);
            }
        } 
        catch (Exception e) 
        {
            //TODO: handle exception
            System.err.println(e.getMessage());
        }
    }

     /**
     * @return gibt den Text zurück.
     */ 
    public String getText()
    {
        return text;
    }

    /**
     * @param bestimmt einen Text t fest.
     * Setzt fest, dass des Text gleich t ist.
     */
    public void setText(String newtext)
    {
        this.text = newtext;
    }

    /**
     * @return gibt die Überschrift des Textes aus.
     */ 
    public String getUeberschrift()
    {
        return this.ueberschrift;
    }

    /**
     * @param ist der Name der Überschrift u.
     * ersetzt die Überschrift mit u.
     */
    public void setUeberschrifft(String u)
    {
        ueberschrift = u;
    }

    /**
     * Schließ die Verbindung zur Datenbank.
     */
    public void closeConnection()
    {
        db.closeConnection();
    }
    
    public int getTextLength()
    {
        return textlaenge;
    }
}
