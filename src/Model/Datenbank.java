package src.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.sql.*;
import java.util.Random;

public class Datenbank
{
    private Connection con;
    private Statement stmt;
    private final String URL = "jdbc:mysql://localhost:3306/racetyper?serverTimezone=Europe/Berlin&autoReconnect=true&useSSL=false";
    private static String USER;
    private static String PASSWORD;
    private static Datenbank instance;
    private String abfrage;

    /**
     * Konstruktor ruft Methode startConnection() auf.
     */
    private Datenbank() 
    {
        this.startConnection();
        this.closeConnection();
    }

    /**
     * @return Gibt die Instance zurück.
    */
    public static Datenbank getInstance() 
    {
        if (instance == null){
            instance = new Datenbank();
            return instance;
        }
        else{        
            return instance;
        }
    }

    /**
     * Funktion für das Überprüfen des Anmeldevorgangs. 
     */
    public static void verbindungsdatenSetzen(String benutzer, String passwort){
        USER = benutzer;
        PASSWORD = passwort;
    }
    
    /**
     * Stellt eine Verbindung zur lokalen Datenbank her.
     */
    private void startConnection() 
    {
        try {
            this.con  = DriverManager.getConnection(this.URL, this.USER, this.PASSWORD);
            this.stmt = con.createStatement();
            System.out.println("Verbindung zur Datenbank wurde erfolgreich hergestellt");
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    /**
     * trennt die Verbindung zur Datenbank
     */
    public void closeConnection() 
    {
        try 
        {
            this.con.close();
            System.out.println("Verbindung zur Datenbank wurde erfolgreich getrennt");
        } 
        catch (Exception e) 
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Gibt die Datenbank tabellarisch in der Konsole aus.
     */
    public void ConsoleAusgeben() 
    {  
        startConnection();
        try 
        {
            // ausgeben
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(abfrage);

            int columns = rs.getMetaData().getColumnCount();
            for(int i = 1; i<=columns; i++)
                System.out.print(String.format("%-15s", rs.getMetaData().getColumnLabel(i)));

            System.out.println();
            System.out.println("----------------------------------------------------------------");

            while(rs.next()) 
            {
                for(int i = 1; i<=columns; i++)
                    System.out.print(String.format("%-15s", rs.getString(i)));
                System.out.println();
            }

            rs.close();
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println("Die Datenbank konnte nicht angezeigt werden");
        }
        closeConnection();
    }

    /**
     * @param ist der aufzurufende SQL Befehl.
     * @return gibt rs oder null aus, jenachdem ob der Befehl ausgeführt werden konnte oder nicht.
     */
    public ResultSet abrufen(String SQLBefehl)
    {
        startConnection();    
        try
        {
            ResultSet rs = stmt.executeQuery(SQLBefehl);
            return rs;
        }
        catch (SQLException e) 
        {
            System.out.println("Eine Verbindung zur Datenbank konnte nicht hergestellt werden");
            return null;
        }
        //disconnection in class Benutzer
    }
    
    
    /**
     * @return gibt die Anzahl an Reihen der Tabelle Benutzer an 
     * @throws SQLException wirft eine SQL Exception bei einem Fehler
     */
    public int getAmountRows() throws SQLException
    {
        startConnection();
        String sql = ("SELECT * FROM racetyper.benutzer;");
        Statement stmt = con.createStatement();
        ResultSet rs =  stmt.executeQuery(sql);
        int id = 0;
        try 
        {
            while(rs.next())
            {
                id = rs.getRow();
                
            }
        } 
        catch (Exception e) 
        {
            System.err.println(e);            
        }
        //closeConnection(); do bad stuff - not needed because con close in insertUser
        return id;
    }

    /**
     * Benutzer sollen in die Tabelle Benutzer der Datenbank eingefügt werden
     * @param bn braucht ein Objekt benutzer
     */
    public void insertUser(Benutzer bn)
    {       
        startConnection();
        Random rand = new Random();
        int timeInGame = rand.nextInt(1000);
        
        try 
        {
            int idBenutzer = this.getAmountRows() + 1; 
            String newSQL = "INSERT INTO racetyper.benutzer(idBenutzer, Username, WPM_Highscore, TimeInGame) values (?,?,?,?)";
            PreparedStatement pstmt =  con.prepareStatement(newSQL, Statement.RETURN_GENERATED_KEYS);

            pstmt.setInt(1, idBenutzer);
            pstmt.setString(2, bn.getName());
            pstmt.setDouble(3, bn.getWPM());
            pstmt.setInt(4, timeInGame);
            
            pstmt.executeUpdate();
            //syso top 10 playes with their name an wpm 
            //System.out.println("User: " + bn.getName() + " ,ID: " + idBenutzer + "was insert with a WPM of" + bn.getWPM() +"and has a TimeInGame" + timeInGame);
        }           
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        //disconnection in class MainMenuGui :=) 
    }  
}
