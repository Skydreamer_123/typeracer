package src.Model;
public class GameState 
{
    
    private String anzuzeigenderText;
    private Text text;
    private int gameState;
    /**
     * @Konstruktor erstellt einen neuen Text
     */
    public GameState()
    {
        text = new Text();
    }

    /**
     * @return holt sich die Länge des Textes
     */ 
    public int anzahlZeichen()
    {
        return text.getTextLength();
    }
    
    /**
     * @return gibt den aktuellen GameState aus
     */
    public int getGameState()
    {
        return gameState;
    }

    /**
     * @param ist der Wert des aktuellen GameStates
     * aktualisiert den GameState
     */
    public void setGameState(int wert)
    {
        gameState = wert;
    }
    
    /**
     * @return Gibt den anzuzeigendenText zurück.
     */
    public String getAnzuzeigenderText()
    {
        text =new Text();
        anzuzeigenderText = text.getText();
        return text.getText();
    }
    
    /**
     * Gibt den gameState 0 zurück.
     */
    public int giveGameState()
    {
        return 0;
    }
    
    /**
     * Überschrift von der Datenbank
     * @return gibt die Überschrift des jeweiligen Textes zurück
     */
    public String getUeberschrift(){
        return this.text.getUeberschrift();
    }

    /**
     * schließ die Verbindung zur Datenbank
     */
    public void closeConnection()
    {
        text.closeConnection();
    }
}
