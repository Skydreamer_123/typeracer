package src.Model;

import java.sql.ResultSet;

import src.View.AnmeldeGUI;

public class Benutzer 
{
    private String name;
    private double wpm;
    private Datenbank db;
    private Benutzer[] benutzers;   

    /**
     * @Konstruktor legt einen neuen Benutzer an mit Namen und freiem Platz für die Tippgeschwindigkeit
     * @param String n = Name des Benutzers
     * @param double w = Tippgeschwindigfkeit in wpm (=words per minute)
     */
    public Benutzer(String newName, double newWpm)
    {
        this.name = newName;
        this.wpm = newWpm;
    }

    /**
     *  @Konstruktor stellt eine Verbindung zur Datenbank her und erstellt den
     */
    public Benutzer()
    {
        db = Datenbank.getInstance();
        benutzers = new Benutzer[10];
        benutzers = this.getTopTen();
    }
    
    /**
     * @return gibt die Tippgeschwindigkeit eines ausgewählten Benutzers zurück
     */
    public double getWPM()
    {
        return wpm;
    }
    
    /**
     * @param ist die Geschwindigkeit des Spielers, die dann als wpm gespeichert wird
     * speichert die Tippgeschwindigkeit ab oder überschreibt gegebenenfalls die schon bestehende Geschwindigkeit
     */
    public void setWPM(double w)
    {
        wpm = w;
    }

    /**
     * @return gibt den Benutzernamen eines ausgewählten Benutzers zurück
     */
    public String getName()
    {
        return name;
    }

    /**
     * speichert die 10 besten Spieler aus der Datenbank in dem Array Benutzer[] ab
     * @return gibt das Array Benutzer zurück
     */
    public Benutzer[] getTopTen()
    {
        resetUsers();
        String abfrage = "select * from benutzer ORDER BY WPM_Highscore DESC LIMIT 10";
        ResultSet rs = db.abrufen(abfrage);
        try {
            while(rs.next())
            {
                String id = rs.getString("idBenutzer");
                this.name = rs.getString("Username");  
                this.wpm = rs.getDouble("WPM_Highscore"); 
                int gameInTime = rs.getInt("TimeInGame");
                // System.out.println(id + " " + name + " " + wpm + " " + gameInTime);
                if(benutzers[rs.getRow()] == null)
                {
                    benutzers[rs.getRow()] = new Benutzer(name,wpm);
                    System.out.println(benutzers[rs.getRow()].getName() + " hat eine Tippgeschwindigkeit von " + benutzers[rs.getRow()].getWPM());
                }
            }
        } catch (Exception e) {
            //TODO: handle exception

        }
        db.closeConnection();
        return benutzers;
    }
    /**
     * @return gibt den lokal abgespeicherten Benutzer Array zurück
     */
    public Benutzer[] getbenutzers()
    {
        return benutzers;
    }

    /**
     * @param bn gibt die Methode weiter an die Klasse Datenbank weiter
     */

    public void insertUser(Benutzer bn)
    {
        db.insertUser(bn);
    }

    public void resetUsers()
    {
        for (int i = 0; i < benutzers.length; i++) {
            benutzers[i] = null;
        }
    }
}
