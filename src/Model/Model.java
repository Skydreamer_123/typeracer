package src.Model;

import src.Controller.GameController;
import src.Controller.MainMenuController;
import src.list.Liste;

import src.View.AnmeldeGUI;

public class Model
{
    private Liste observerListe = new Liste();
    private MainMenuController mainMenuObserver; 
    private GameController gameObserver; 
    private GameState gamestate;
    private HighscoreState hState;
    private int gameState;
    private String text;
    private int fehlerCount = 0;
    private int letzteZeit = 0;
    private double geschwindigkeit;
    private AnmeldeGUI anGUI;

    /**
     * @Konstruktor erstellt die beiden Controller und fügt sie zur observerListe hinzu.
     */
    public Model(String benutzer, String passwort)
    {
        Datenbank.verbindungsdatenSetzen(benutzer, passwort);
        mainMenuObserver = new MainMenuController(this);
        gameObserver = new GameController(this);
        observerListe.attach(mainMenuObserver);
        observerListe.attach(gameObserver);
        gamestate = new GameState(); 
        hState = new HighscoreState();  
        notifyObserver();
    }

    /**
     * Ruft die Methode notifyObserver bei der Liste auf.
     */
    public void notifyObserver()
    {
        observerListe.notifyObserver();    
    }

    /**
     * @return Gibt den gameState zurück.
     */
    public int getGameState()    
    {
        return gamestate.getGameState();    
    }
    
    public void backToMenu()
    {
        this.gamestate.setGameState(0);
        this.notifyObserver();
    }
    
    public String getUeberschrift()
    {
        return this.gamestate.getUeberschrift();
    }

    /**
     * Setzt den gameState auf 1 und benachrichtigt die Observer. (gGUI wird angezeigt)
     */
    public void mainMenuStartAction()
    {   
        gamestate.setGameState(1);
        this.text = gamestate.getAnzuzeigenderText();
        fehlerCount = 0;
        notifyObserver();                
    }

    /**
     * Der Buchstabe wird mit dem ersten Buchstaben des Textes überprüft.
     * @param Der Buchstabe wird mit dem ersten Buchstaben des Textes überprüft.
     * Fehlt: else-Schleife, die die Engabe eines weiteren Buchstaben sperrt und auf rot stellt.
     * Falls kein Buchstabe mehr vorhanden ist, wird der gameState auf 0 gestellt (mmGUI wird angezeigt). (alle)
     */
    public void textVergleich(String buchstabe)
    {
        if(this.text.length()>1) //Vorher 1
        {
            try
            {
                String momentanerBuchstabe = this.text.substring(0,1);
                if(buchstabe.equals(momentanerBuchstabe))
                {
                    this.text = this.text.substring(1,this.text.length());
                    
                } 
                else 
                {
                    fehlerMehr();
                }
            }
            catch(IndexOutOfBoundsException e)
            {
                System.out.println("Zu weit");
            }   
            
        }
        else
        {   
            letzteZeit = observerListe.getTimer();
            gamestate.setGameState(0);
            notifyObserver();
            System.out.println("Text wurde fertig abgeschrieben");
            
        }
        notifyObserver();
    }

    /**
     * @return Gibt den Text zurück.
     */
    public String getText()
    {
        return text;    
    }

    /**
     * Zählt die Fehler und reduziert die Anzahl um eins, wenn ein Fehler gelöscht wurde
     */
    public void fehlerWeniger()
    {
        if(this.fehlerCount > 0)
        {
            this.fehlerCount = this.fehlerCount - 1;
        }
    }

    /**
     * Zählt die Fehler und addiert einen, wenn ein weiterer gemacht wurde
     */
    public void fehlerMehr()
    {
        this.fehlerCount = this.fehlerCount + 1;
    }

    /**
     * @return gibt die Anzahl der gemachten Fehler aus
     */
    public int getFehler()
    {
        return this.fehlerCount;
    }

    /**
     * @return gibt die letzte Zeit, die für einen TExt gebraucht wurde aus
     */
    public int getLetzteZeit()
    {
        return letzteZeit;
    }

    /**
     * @return holt sich die Anzahl der Zeichen bei GameState
     */
    public int getAnschlaege()
    {
        return gamestate.anzahlZeichen();  
    }

    /**
     * @return berechnet die Tippgeschwindigkeit in Zeichen pro Minute
     */
    public double getGeschwindigkeit()
    {
        if(letzteZeit > 0)
        {
            double anschlaege = gamestate.anzahlZeichen();
            double geschwindigkeit = ((anschlaege / letzteZeit)*60);
            geschwindigkeit = Math.round(geschwindigkeit*100)/100.00;
            return geschwindigkeit;
        }
        else
        {
            return -10;
        }
    }
    
    /**
     * erstellt einen Benutzer und gibt ihn zurück
     * @return gibt den benutzer in Form eines Objekts zurück.
     */
    public Benutzer benutzerGeben()
    {
        double geschwindigkeit = this.getGeschwindigkeit();
        Benutzer bn = new Benutzer(AnmeldeGUI.benutzernameAusgeben(), geschwindigkeit);
        return bn;        
    }

    /**
     * fügt den einen Benutzer in die Datenbank ein
     * @param bn Obkekt der Klasse Benutzer 
     */
    public void insertUser(Benutzer bn)
    {
        hState.insertUser(bn);
    }

    

    /**
     * schließ die Verbindung zur Datenbank
     */
    public void closeConnection()
    {
        this.gamestate.closeConnection();
    }

    /*--------------------------------------------------------------------------------------------
    *                                       for BestListGui
    *--------------------------------------------------------------------------------------------*/
    
    /**
     * ruft die Methode bei HighscoreState auf.
     * @return gibt die 10 besten Spieler aus der Datenbank zurück.
     */
    public Benutzer[] getTopTen()
    {
        return this.hState.getTopTen();
    }
}
