package src.Model;

public class HighscoreState 
{
    
    private Benutzer benutzer;

    /**
     * @Konstruktor ist der Konstruktor der Klasse HighscoreState
     */
    public HighscoreState()
    {
        this.benutzer = new Benutzer();
    }
    
    /**
     *  zeigt die TopTen der Spieler an
     */
    public Benutzer[] getTopTen()
    {
        return benutzer.getTopTen();
    }

    /**
     * fügt den einen Benutzer in die Datenbank ein
     * @param bn Obkekt der Klasse Benutzer 
     */
    public void insertUser(Benutzer bn)
    {
        benutzer.insertUser(bn);
    }
}
