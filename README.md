PROJEKTBEZEICHNUNG: RACETYPER
PROJEKTZWECK: Übung zur Entwicklung eines Programmes als Teamprojekt; Inhalt des G8-Lehrplans Informatik 11 Bayern
VERSION oder DATUM: 29.06.2021
WIE IST DAS PROJEKT ZU STARTEN: Die Datei TypeAndRun.jar ausführen
AUTOR(EN): Informatik-Kurs der Q11 GMI 2020/21
BENUTZERHINWEISE: Es ist eine mySQL-Datenbank zu initialisieren. Tabellen sind aus dem Ordner "database" zu entnehmen, die Datenbank muss unter Port 3306 lokal laufen. Benutzerdaten werden zu Beginn der Anwendung abgefragt.
