-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: racetyper
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `texte`
--

DROP TABLE IF EXISTS `texte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `texte` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ueberschrift` varchar(5000) NOT NULL,
  `text` varchar(9000) NOT NULL,
  `textlaenge` int NOT NULL,
  `schwierigkeit` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `texte`
--

LOCK TABLES `texte` WRITE;
/*!40000 ALTER TABLE `texte` DISABLE KEYS */;
INSERT INTO `texte` VALUES (1,'Die Zugspitzbahn','Die Doppeltriebwagen 12 bis 16 der Bayerischen Zugspitzbahn sind meterspurige Elektrotriebwagen für den gemischten Adhäsions- und Zahnradbetrieb, die von Stadler Rail geliefert wurden.',184,3),(2,'Das Gute','Oft verliert man das Gute, wenn man das Bessere sucht.',54,5),(3,'Wunder','Es gibt nur zwei Arten zu leben. Entweder so als wäre nichts ein Wunder oder so als wäre alles ein Wunder.',106,9),(4,'Schlaue Lehrer','Nichts ist schrecklicher als ein Lehrer, der nicht mehr weiß als das, was die Schüler wissen sollen.',100,8),(5,'Wissen','Zu wissen, was man weiß, und zu wissen, was man tut, das ist Wissen.',68,7),(6,'Glück','Wenn man glücklich ist, sollte man nicht noch glücklicher sein wollen.',70,8),(7,'Hoffnung','Wer nichts wagt, der darf nichts hoffen.',40,6),(8,'Reise','Auch eine Reise von tausend Meilen beginnt mit einem Schritt.',61,4),(9,'Träume','Wenn wir träumen, betreten wir eine Welt, die ganz und gar uns gehört.',70,6),(10,'Träume II','Alle Träume können wahr werden, wenn wir den Mut haben, ihnen zu folgen.',72,2),(11,'Veränderung','Beklage nicht, was nicht zu ändern ist, aber ändere, was zu beklagen ist.',73,5),(12,'Die Idee','Nichts auf der Welt ist so mächtig wie eine Idee, deren Zeit gekommen ist.',74,7),(13,'Entscheidungen','Viel mehr als unsere Fähigkeiten sind es unsere Entscheidungen, die zeigen, wer wir wirklich sind.',98,5),(14,'Hindernisse','Hindernisse können mich nicht aufhalten; Entschlossenheit bringt jedes Hindernis zu Fall.',89,6),(15,'Erfolg','Unsere größte Schwäche liegt im Aufgeben. Der sichere Weg zum Erfolg ist immer, es doch noch einmal zu versuchen.',113,6),(16,'Erfolg II','Der Erfolg ist nicht danach zu beurteilen, was ein Mensch im Leben erreicht, sondern nach den Hindernissen, die er auf dem Weg zum Erfolg überwunden hat.',153,6),(17,'Sammelbilder','Drei Kinder sind wegen Fußballsammelbildern in einen Supermarkt eingebrochen und haben dabei laut Polizei einen Sachschaden von rund 10.000 Euro verursacht. Die Jungen im Alter von acht bis zwölf Jahren hätten sich am Sonntagabend in einem Einkaufsmarkt im südhessischen Kelsterbach bei Frankfurt mit großen Steinen Zugang verschafft, teilte die Polizei am Montag mit.',368,7);
/*!40000 ALTER TABLE `texte` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-17 20:14:47
